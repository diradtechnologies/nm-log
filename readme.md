# Log

Logging Module to simplify DiRAD Logging Utility

### Add to your code

This is not added as a usual npm module as it is not listed on npm. The code is stored publicly in our bitBucket. 

To add the code to your project add the folowing to your dependencies in your package.json file. 

```json
{
"log": "git+https://bitbucket.org/diradtechnologies/nm-log.git"
}
```

It can then be used as a normal module at the top of your code.

```javascript
const log = require('log')
```

## Usage
const moment = require('moment'); //Manages time 
const http = require('request'); //handles http
const fs = require('fs'); //File system used to read config file
const crypto = require('crypto') //Hashing passwords, random data,


var serverID
var loggingData = {
    loggerAddress: "",
    loggerUserName: "",
    loggerPass: "",
    logToEmail: "",
    logFromEmail: "",
    domain: "",
    appLocation: "",
    sendgridkey: "",
    environment: "",
    application: "",
    server: ""
}
var logText = "\x1b[90m" + "LOG -- " + "\x1b[0m"

var logsToWrite = []
var redundantMode = false;
var redudantServers = [];


module.exports = {
    init: function (loggingPayload) {
        if (loggingPayload.hasOwnProperty('loggerAddress') &&
            loggingPayload.hasOwnProperty('loggerUserName') &&
            loggingPayload.hasOwnProperty('loggerPass') &&
            loggingPayload.hasOwnProperty('logToEmail') &&
            loggingPayload.hasOwnProperty('logFromEmail') &&
            loggingPayload.hasOwnProperty('domain') &&
            loggingPayload.hasOwnProperty('appLocation') &&
            loggingPayload.hasOwnProperty('sendgridkey') &&
            loggingPayload.hasOwnProperty('environment') &&
            loggingPayload.hasOwnProperty('application') &&
            loggingPayload.hasOwnProperty('server')) {
            //check if server is an array
            console.log(logText + '\x1b[32m' + '@@@@@INIT' + '\x1b[0m')
            console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + ((Array.isArray(loggingPayload.loggerAddress)) ? 'Redundant Mode' : 'Single Mode'))
            console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + "Server Info:" + JSON.stringify(loggingPayload.loggerAddress))
            console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + "Current Time: " + moment().format("YYYY-MM-DD HH:mm:ss"))

            if (Array.isArray(loggingPayload.loggerAddress)) {

                redundantMode = true;
                loggingData = loggingPayload
                for (var i = 0; i < loggingPayload.loggerAddress.length; i++) {
                    //check if server is up 
                    checkServerStatus({ server: loggingPayload.loggerAddress[i] }, loggingPayload, (function (active, server) {
                        redudantServers.push({ server: server.server, active: active, master: false })
                        if (active) {
                            //check if it is the first active server in redudantServers
                            if (redudantServers[0].server == server.server) {
                                console.log(logText + '\x1b[32m' + 'Master Server Active: ' + '\x1b[0m' + server.server)

                                serverID = newID();
                                redudantServers[0].master = true;

                                var options = { //Options to Post call data to the logger
                                    method: 'POST', //Post to log data
                                    uri: redudantServers[0].server, //Address to hit the logger, provided by the config file
                                    headers: {
                                        'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' +
                                            loggingData.loggerPass)).toString(
                                                "base64") //Authication information for the logger provided by the config file
                                    },
                                    body: { //data for logger
                                        application: loggingData.application,
                                        reference: serverID,
                                        timeOut: 3600000,
                                        data: {
                                            connection: {
                                                localAddress: loggingData.appLocation,
                                                reference: serverID,
                                                environment: loggingData.environment,
                                                server: loggingData.server,
                                                start: new Date().toJSON(),
                                                type: '3',
                                                direction: '1'
                                            }
                                        }
                                    },
                                    json: true
                                };
                                http(options, (err, response, body) => {
                                    if (err) {
                                        console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                                        console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + redudantServers[0])
                                        console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + err)
                                        logsToWrite.push(options)
                                    }
                                    else if (response.statusCode != 200) {
                                        console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                                        console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + response.request.uri.href)
                                        console.log(logText + '\x1b[31m' + '~XX~ Status Code: ' + '\x1b[0m' + response.statusCode)
                                        console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + body)
                                        logsToWrite.push(options)
                                    } else {
                                        console.log(logText + '\x1b[32m' + '@@@@@Logging Initiated' + '\x1b[0m')
                                        console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + serverID)
                                        console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + moment().format("YYYY-MM-DD HH:mm:ss"))//Current Time
                                        console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + response.request.uri.href)
                                    }
                                })
                            } else {
                                console.log(logText + '\x1b[33m' + 'Redundant Server: ' + '\x1b[0m' + server.server)
                            }
                        } else {
                            console.log(logText + '\x1b[31m' + 'Server Inavtice: ' + '\x1b[0m' + server.server)

                        }
                    }))
                }
            } else {
                loggingData = loggingPayload
                serverID = newID();
                var options = { //Options to Post call data to the logger
                    method: 'POST', //Post to log data
                    uri: loggingData.loggerAddress, //Address to hit the logger, provided by the config file
                    headers: {
                        'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' +
                            loggingData.loggerPass)).toString(
                                "base64") //Authication information for the logger provided by the config file
                    },
                    body: { //data for logger
                        application: loggingData.application,
                        reference: serverID,
                        timeOut: 3600000,
                        data: {
                            connection: {
                                localAddress: loggingData.appLocation,
                                reference: serverID,
                                environment: loggingData.environment,
                                server: loggingData.server,
                                start: new Date().toJSON(),
                                type: '3',
                                direction: '1'
                            }
                        }
                    },
                    json: true
                };
                http(options, (err, response, body) => {
                    if (err) {
                        console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                        console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + loggingData.loggerAddress)
                        console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + err)
                        logsToWrite.push(options)
                    }
                    else if (response.statusCode != 200) {
                        console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                        console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + loggingData.loggerAddress)
                        console.log(logText + '\x1b[31m' + '~XX~ Status Code: ' + '\x1b[0m' + response.statusCode)
                        console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + body)
                        logsToWrite.push(options)
                    } else {
                        console.log(logText + '\x1b[32m' + '@@@@@Logging Initiated' + '\x1b[0m')
                        console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + serverID)
                        console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + moment().format("YYYY-MM-DD HH:mm:ss"))//Current Time
                        console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + loggingPayload.loggerAddress)
                    }
                })
            }
        } else {
            console.error('Missing Logging Data')
        }
    },
    server: function (payload) {
        if (payload.email) {
            errorQueue.push({
                time: moment().toString(),
                message: payload.textshort,
                messageLong: payload.textlong,
                location: payload.location
            })
        }
        console.log(logText + '\x1b[33m' + '@@@@@Server Log' + '\x1b[0m')
        console.log(logText + '\x1b[33m' + '@@@@ ' + '\x1b[0m' + payload.textshort)
        console.log(logText + '\x1b[33m' + '@@@@ ' + '\x1b[0m' + payload.textlong)
        console.log(logText + '\x1b[33m' + '@@@@ ' + '\x1b[0m' + 'Location: ' + payload.location)

        if (redundantMode) {
            //find master server 
            var masterServer = redudantServers.find(server => server.master === true)
            if (masterServer) {
                var options = { //Options to Post call data to the logger
                    method: 'PUT', //Post to log data
                    uri: masterServer.server, //Address to hit the logger, provided by the config file
                    headers: {
                        'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' +
                            loggingData.loggerPass)).toString(
                                "base64") //Authication information for the logger provided by the config file
                    },
                    body: { //data for logger
                        application: loggingData.application,
                        reference: serverID,
                        timeOut: 3600000,
                        data: {
                            flow: {
                                dateTime: new Date().toJSON(),
                                action: '11',
                                location: payload.location.split(':')[payload.location.split(':').length - 2],
                                response: payload.textshort,
                                trace: payload.trace,
                                '7': payload.textlong
                            }
                        }
                    },
                    json: true
                };

                http(options, (err, response, body) => {
                    if (err) {

                        console.log(logText + '\x1b[31m' + '~XX~ FAILED TO SEND SEVER LOG' + '\x1b[0m')
                        console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + masterServer.server)
                        console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + err)                        //Add to write logs queue
                        logsToWrite.push(options)
                    } else {
                        if (response.statusCode == '200') {
                            console.log(logText + '\x1b[32m' + 'Success: ' + '\x1b[0m' + masterServer.server);
                        } else {

                            console.log(logText + '\x1b[31m' + '~XX~ FAILED LOGS' + '\x1b[0m')
                            console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + masterServer.server)
                            console.log(logText + '\x1b[31m' + '~XX~ Status Code: ' + '\x1b[0m' + response.statusCode)
                            console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + body)
                            logsToWrite.push(options)

                        }

                    }
                })
            } else {
                console.log(logText + '\x1b[31m' + '~XX~ NO MASTER - WRITING LOGS' + '\x1b[0m')
                console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + loggingData.loggerAddress)
                logsToWrite.push(options)
            }
        } else {
            var options = { //Options to Post call data to the logger
                method: 'PUT', //Post to log data
                uri: loggingData.loggerAddress, //Address to hit the logger, provided by the config file
                headers: {
                    'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' +
                        loggingData.loggerPass)).toString(
                            "base64") //Authication information for the logger provided by the config file
                },
                body: { //data for logger
                    application: loggingData.application,
                    reference: serverID,
                    timeOut: 3600000,
                    data: {
                        flow: {
                            dateTime: new Date().toJSON(),
                            action: '11',
                            location: payload.location.split(':')[payload.location.split(':').length - 2],
                            response: payload.textshort,
                            trace: payload.trace,
                            '7': payload.textlong
                        }
                    }
                },
                json: true
            };

            http(options, (err, response, body) => {
                if (err) {

                    console.log(logText + '\x1b[31m' + '~XX~ FAILED TO SEND SEVER LOG' + '\x1b[0m')
                    console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + loggingData.loggerAddress)
                    console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + err)
                    //Add to write logs queue
                    logsToWrite.push(options)
                } else {
                    if (response.statusCode == '200') {
                        console.log(logText + '\x1b[32m' + 'Success: ' + '\x1b[0m' + loggingData.loggerAddress);
                    } else {

                        console.log(logText + '\x1b[31m' + '~XX~ FAILED LOGS' + '\x1b[0m')
                        console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + loggingData.loggerAddress)
                        console.log(logText + '\x1b[31m' + '~XX~ Status Code: ' + '\x1b[0m' + response.statusCode)
                        console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + body)
                    }

                }
            })
        }






    },
    send: function (payload) {

        console.log(logText + '\x1b[34m' + '@@@@@System Log' + '\x1b[0m')
        console.log(logText + '\x1b[34m' + '@@@@ ' + '\x1b[0m' + payload.method)
        console.log(logText + '\x1b[34m' + '@@@@ ' + '\x1b[0m' + moment().format("YYYY-MM-DD HH:mm:ss"))//Current Time
        console.log(logText + '\x1b[34m' + '@@@@ ' + '\x1b[0m' + payload.body.reference)
        if (redundantMode) {
            //get active servers from redudantservers array
            //check for master server 
            var masterServer = redudantServers.find(server => server.master === true)
            if (!masterServer) {
                var options = { //Options to Post call data to the logger
                    method: payload.method, //Post to log data
                    uri: loggingData.loggerAddress, //Address to hit the logger, provided by the config file
                    headers: {
                        'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' + loggingData.loggerPass)).toString("base64") //Authication information for the logger provided by the config file
                    },
                    body: payload.body,
                    json: true
                };
                console.log(logText + '\x1b[31m' + '~XX~ NO MASTER - WRITING LOGS' + '\x1b[0m')
                console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + loggingData.loggerAddress)
                logsToWrite.push(options)
            } else {
                var activeServers = redudantServers.filter(function (server) {
                    return server.active == true;
                });
                //loop through active servers and send logs
                for (var server of activeServers) {
                    payload.body.copy = (!server.master) ? true : false;
                    var options = { //Options to Post call data to the logger
                        method: payload.method, //Post to log data
                        uri: server.server, //Address to hit the logger, provided by the config file
                        headers: {
                            'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' + loggingData.loggerPass)).toString("base64") //Authication information for the logger provided by the config file
                        },
                        body: payload.body,
                        json: true
                    };
                    http(options, (err, response, body) => {
                        if (err) {
                            console.log(logText + '\x1b[31m' + '~XX~ FAILED LOGS' + '\x1b[0m')
                            console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + server.server)
                            console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + err)
                            logsToWrite.push(options)
                            //Add to write logs queue
                        } else {
                            if (response.statusCode == '200') {
                                console.log(logText + '\x1b[32m' + 'Success: ' + '\x1b[0m' + response.request.uri.href);
                            } else {
                                console.log(logText + '\x1b[31m' + '~XX~ FAILED LOGS' + '\x1b[0m')
                                console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + response.request.uri.href)
                                console.log(logText + '\x1b[31m' + '~XX~ Status Code: ' + '\x1b[0m' + response.statusCode)
                                console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + body)
                            }

                        }
                    })
                }
            }




        } else {
            var options = { //Options to Post call data to the logger
                method: payload.method, //Post to log data
                uri: loggingData.loggerAddress, //Address to hit the logger, provided by the config file
                headers: {
                    'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' +
                        loggingData.loggerPass)).toString(
                            "base64") //Authication information for the logger provided by the config file
                },
                body: payload.body,
                json: true
            };

            http(options, (err, response, body) => {
                if (err) {
                    console.log(logText + '\x1b[31m' + '~XX~ FAILED LOGS' + '\x1b[0m')
                    console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + loggingData.loggerAddress)
                    console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + err)
                    logsToWrite.push(options)
                    //Add to write logs queue
                } else {
                    if (response.statusCode == '200') {
                        console.log(logText + '\x1b[32m' + 'Success: ' + '\x1b[0m' + response.request.uri.href);
                    } else {
                        console.log(logText + '\x1b[31m' + '~XX~ FAILED LOGS' + '\x1b[0m')
                        console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + response.request.uri.href)
                        console.log(logText + '\x1b[31m' + '~XX~ Status Code: ' + '\x1b[0m' + response.statusCode)
                        console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + body)
                        logsToWrite.push(options)
                    }


                }
            })
        }
    },
    console: function (payload) {
        console.log(logText + payload)
    },
    location: function () {
        var err = new Error();
        var caller_line = err.stack.split("\n")[2];
        var index = caller_line.indexOf("at ");
        var clean = caller_line.slice(index + 2, caller_line.length);
        return clean
    },
    queue: function () {
        return errorQueue
    }
}

var serverID
//postServerLogs();

var lastEmail = moment().subtract(1, 'days');
var errorQueue = []
setInterval(function () {
    //Send emails
    if (moment().diff(lastEmail, 'minutes') > 5 && !(errorQueue.length == 0)) { //Check if we have sent an email in the last 5 minutes
        lastEmail = moment();                      //Reset the timer
        var emailBody = 'App Address: ' + loggingData.appLocation + '\r\n\r\n' +
            'Email Sent: ' + moment().toString() + '\r\n\r\n' +
            'Following messages logged in the last 5 minutes: \r\n\r\n'
            + JSON.stringify(errorQueue, null, 4);
        errorQueue = [];
        http.post({
            url: 'https://api.sendgrid.com/v3/mail/send',
            auth: {
                'bearer': loggingData.sendgridkey
            },
            json: {
                personalizations: [{
                    to: [{
                        email: loggingData.logToEmail
                    }]
                }],
                from: {
                    email: loggingData.logFromEmail
                },
                subject: 'Status update from ' + loggingData.domain,
                content: [{
                    type: "text/plain",
                    value: emailBody
                }]
            }
        }, (err, httpResponse, body) => {
            if (httpResponse.statusCode != '202') {
                //bad
                console.log(logText + '\x1b[31m', 'Sendgrid Email Failed',
                    '\x1b[0m')
                console.log(logText + 'status: ' + httpResponse.statusMessage)
                console.log(logText + 'code: ' + httpResponse.statusCode)
                //Send to logger
                //Place in retry queue

            }
        })
    }
}, 10000); //Check Every 10 Seconds
setInterval(function () {
    var dataToLog = JSON.parse(JSON.stringify(logsToWrite));
    logsToWrite = [];
    dataToLog.forEach(function (data) { console.log(logText + '\x1b[31m' + '\x1b[4m' + ">>Failed Log>>", new Date().toISOString() + ">>" + (JSON.stringify(data)), '\x1b[0m') });
}, 30000); //Check Every 30 seconds
setInterval(function () {
    serverID = newID();
    if (redundantMode) {
        //find master server 
        var masterServer = redudantServers.find(server => server.master === true)
        var options = { //Options to Post call data to the logger
            method: 'POST', //Post to log data
            uri: masterServer.server, //Address to hit the logger, provided by the config file
            headers: {
                'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' +
                    loggingData.loggerPass)).toString(
                        "base64") //Authication information for the logger provided by the config file
            },
            body: { //data for logger
                application: loggingData.application,
                reference: serverID,
                timeOut: 3600000,
                data: {
                    connection: {
                        localAddress: loggingData.appLocation,
                        reference: serverID,
                        environment: loggingData.environment,
                        server: loggingData.server,
                        start: new Date().toJSON(),
                        type: '3',
                        direction: '1'
                    }
                }
            },
            json: true
        };
        http(options, (err, response, body) => {
            if (err) {
                console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + masterServer.server)
                console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + err)
                logsToWrite.push(options)
            }
            else if (response.statusCode != 200) {
                console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + masterServer.server)
                console.log(logText + '\x1b[31m' + '~XX~ Status Code: ' + '\x1b[0m' + response.statusCode)
                console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + body)
                logsToWrite.push(options)
            } else {
                console.log(logText + '\x1b[32m' + '@@@@@Logging Initiated' + '\x1b[0m')
                console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + serverID)
                console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + moment().format("YYYY-MM-DD HH:mm:ss"))//Current Time
                console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + masterServer.server)
            }
        })
    }
    else {
        var options = { //Options to Post call data to the logger
            method: 'POST', //Post to log data
            uri: loggingData.loggerAddress, //Address to hit the logger, provided by the config file
            headers: {
                'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' +
                    loggingData.loggerPass)).toString(
                        "base64") //Authication information for the logger provided by the config file
            },
            body: { //data for logger
                application: loggingData.application,
                reference: serverID,
                timeOut: 3600000,
                data: {
                    connection: {
                        localAddress: loggingData.appLocation,
                        reference: serverID,
                        environment: loggingData.environment,
                        server: loggingData.server,
                        start: new Date().toJSON(),
                        type: '3',
                        direction: '1'
                    }
                }
            },
            json: true
        };
        http(options, (err, response, body) => {
            if (err) {
                console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + err)
                logsToWrite.push(options)
            }
            else if (response.statusCode != 200) {
                console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                console.log(logText + '\x1b[31m' + '~XX~ Status Code: ' + '\x1b[0m' + response.statusCode)
                console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + body)
                logsToWrite.push(options)
            } else {
                console.log(logText + '\x1b[32m' + '@@@@@Logging Initiated' + '\x1b[0m')
                console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + serverID)
                console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + moment().format("YYYY-MM-DD HH:mm:ss"))//Current Time
            }
        })
    }



}, 3600000);
setInterval(function () {
    //for each server in the list check server status 
    if (redundantMode) {
        for (var i = 0; i < redudantServers.length; i++) {
            checkServerStatus(redudantServers[i], loggingData, function (status, server) {
                if (status) {
                    if (server.active === false) {
                        console.log(logText + '\x1b[32m', server.server, 'is up ✅', '\x1b[0m')
                    }
                    server.active = true;
                    //console.log(logText + '\x1b[32m', server.server, 'is up ✅', '\x1b[0m')
                } else {
                    if (server.master) {
                        console.log(logText + '\x1b[31m', server.server, 'is down ❌ -- MASTER SERVER', '\x1b[0m')
                    } else {
                        console.log(logText + '\x1b[33m', server.server, 'is down ❌', '\x1b[0m')
                    }
                    //Check if server was the master
                    server.master = false;
                    server.active = false;
                }
            })
        }
    }

}, 1000); //Check Every 10 Seconds
setInterval(function () {
    //check for master server in redudant servers
    if (redundantMode) {
        var masterServer = redudantServers.find(function (server) {
            return server.master;
        }
        );
        if (!masterServer) {
            //set first active server to master
            var activeServer = redudantServers.find(function (server) {
                return server.active;
            }
            );
            if (activeServer) {
                serverID = newID();
                activeServer.master = true;
                console.log(logText + '\x1b[32m', 'New Master Server: ', activeServer.server, '\x1b[0m')
                var masterServer = redudantServers.find(server => server.master === true)
                var options = { //Options to Post call data to the logger
                    method: 'POST', //Post to log data
                    uri: activeServer.server, //Address to hit the logger, provided by the config file
                    headers: {
                        'Authorization': 'Basic ' + new Buffer.from((loggingData.loggerUserName + ':' +
                            loggingData.loggerPass)).toString(
                                "base64") //Authication information for the logger provided by the config file
                    },
                    body: { //data for logger
                        application: loggingData.application,
                        reference: serverID,
                        timeOut: 3600000,
                        data: {
                            connection: {
                                localAddress: loggingData.appLocation,
                                reference: serverID,
                                environment: loggingData.environment,
                                server: loggingData.server,
                                start: new Date().toJSON(),
                                type: '3',
                                direction: '1'
                            }
                        }
                    },
                    json: true
                };
                http(options, (err, response, body) => {
                    if (err) {
                        console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                        console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + activeServer.server)
                        console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + err)
                        logsToWrite.push(options)
                    }
                    else if (response.statusCode != 200) {
                        console.log(logText + '\x1b[31m' + '~XX~ FAILED TO INITIATE LOGGING' + '\x1b[0m')
                        console.log(logText + '\x1b[31m' + '~XX~ Logger Address: ' + '\x1b[0m' + activeServer.server)
                        console.log(logText + '\x1b[31m' + '~XX~ Status Code: ' + '\x1b[0m' + response.statusCode)
                        console.log(logText + '\x1b[31m' + '~XX~ Error Message: ' + '\x1b[0m' + body)
                        logsToWrite.push(options)
                    } else {
                        console.log(logText + '\x1b[32m' + '@@@@@Logging Initiated' + '\x1b[0m')
                        console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + serverID)
                        console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + moment().format("YYYY-MM-DD HH:mm:ss"))//Current Time
                        console.log(logText + '\x1b[32m' + '@@@@ ' + '\x1b[0m' + activeServer.server)
                    }
                })
            }
        }
    }

}, 500); //Check Every 10 Seconds

function checkServerStatus(server, data, callback) {
    var options = {
        method: 'GET',
        uri: server.server + 'heartbeat',
        timeout: 5000,
        headers: {
            'Authorization': 'Basic ' + new Buffer.from((data.loggerUserName + ':' + data.loggerPass)).toString("base64") //Authication information for the logger provided by the config file
        },
    };
    http(options, (err, response, body) => {
        if (err || response.statusCode != 200) {
            callback(false, server);
        } else {
            callback(true, server);
        }
    })
}



function newID() {
    var hexstring = crypto.randomBytes(10).toString("hex");
    return [hexstring.substring(0, 19), new Date().getTime().toString()].join('');
}